﻿using express.bttree;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace MySqlDataView.Logic
{
    class ListViewItemTextConvert : IValueConverter
    {
        private Node exprNode;

        public ListViewItemTextConvert(Node exprNode)
        {
            this.exprNode = exprNode;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Context context = new Context();
            context.Args = value;
            var state = this.exprNode.execute(context);
            if(state == State.True)
            {
                return context.getResultAtIndex(0);
            } 
            else
            {
                return string.Empty;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
