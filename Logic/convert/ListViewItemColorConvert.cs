﻿using express.bttree;
using MySqlDataView.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace MySqlDataView.Logic
{
    class ListViewItemColorConvert : IValueConverter
    {
        private Node exprIsHighlightNode;

        public ListViewItemColorConvert(Node exprIsHighlightNode)
        {
            this.exprIsHighlightNode = exprIsHighlightNode;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Context context = new Context();
            context.Args = value;
            var state = this.exprIsHighlightNode.execute(context);
            if (state == State.True)
            {
                return Brushes.Red;
            }
            else
            {
                //默认的颜色
                return Consts.DEFAULT_TEXT_COLOR;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
