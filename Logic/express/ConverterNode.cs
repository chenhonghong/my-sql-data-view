﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using express;
using express.bttree;
using express.compiler;

namespace MySqlDataView.Logic.express
{
    public class ConverterNode : OpNode
    {
        private static readonly global::log4net.ILog logger = global::log4net.LogManager.GetLogger("log");

        private static Dictionary<string, Command> commands = new Dictionary<string, Command>();

        static ConverterNode()
        {
            commands.Add("Timestamp2StrTime", Commands.Timestamp2StrTime);
        }

        protected override Dictionary<string, Command> GetCommands()
        {
            return commands;
        }

        public override State execute(Context context)
        {
            if (this.Method == null || this.Value == null || this.Op == Operator.nil)
            {
                return State.False;
            }

            var commands = GetCommands();
            Command command;
            bool found = commands.TryGetValue(this.Method, out command);
            if(!found)
            {
                logger.Error("命令未定义，command:" + this.Method);
                return State.False;
            }

            try
            {
                Object result = command(context, new ParamList(this.Params));
                context.AddResult(result);
                return State.True;
            }
            catch (Exception ex)
            {
                logger.Error("反射调用命令错误，command:" + this.Method, ex);
                return State.False;
            }

        }
    }
}
