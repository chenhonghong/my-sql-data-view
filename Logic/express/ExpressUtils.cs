﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using express.bttree;
using express.compiler;

namespace MySqlDataView.Logic.express
{
    public class ExpressUtils
    {
        public static readonly global::log4net.ILog logger = global::log4net.LogManager.GetLogger("log");

        public static Node parseConverter(string expr)
        {
            try
            {
                return ExprUtils.convert(expr, typeof(ConverterNode));
            }
            catch (Exception ex)
            {
                logger.Error("表达式解析错误，expr:" + expr, ex);
                return null;
            }
        }

        public static Node parseConditions(string expr)
        {
            try
            {
                return ExprUtils.convert(expr, typeof(ConditionsNode));
            }
            catch (Exception ex)
            {
                logger.Error("表达式解析错误，expr:" + expr, ex);
                return null;
            }
        }
    }
}
