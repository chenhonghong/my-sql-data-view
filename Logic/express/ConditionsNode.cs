﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using express;
using express.bttree;
using express.compiler;

namespace MySqlDataView.Logic.express
{
    public class ConditionsNode : OpNode
    {
        private static readonly global::log4net.ILog logger = global::log4net.LogManager.GetLogger("log");

        private static Dictionary<string, Command> commands = new Dictionary<string, Command>();

        static ConditionsNode()
        {
            commands.Add("IsHighlight", Commands.IsHighlight);
        }

        protected override Dictionary<string, Command> GetCommands()
        {
            return commands;
        }
    }
}
