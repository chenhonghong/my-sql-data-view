﻿using express.bttree;
using K4os.Compression.LZ4.Streams.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace MySqlDataView.Logic.express
{
    public class Commands
    {
        private static readonly global::log4net.ILog logger = global::log4net.LogManager.GetLogger("log");


        private static string GetValue(Object arg, string colName)
        {
            var data = arg as System.Dynamic.ExpandoObject;
            if (data == null)
            {
                logger.Error("调用参数类型错误，args:" + arg);
                return string.Empty;
            }

            foreach (var item in data)
            {
                if (string.Equals(item.Key, colName))
                {
                    return item.Value.ToString();
                }
            }

            return null;
        }

        #region 条件类型
        public static Object IsHighlight(Context ctx, ParamList paramList)
        {
            if (ctx.Args == null || paramList.ParamCount < 2)
            {
                logger.Error("参数错误");
                return Common.Consts.DEFAULT_TEXT_COLOR;
            }

            var colName = paramList.GetParamAsString(0) ?? "";
            var matchText = paramList.GetParamAsString(1) ?? "";

            var value = GetValue(ctx.Args, colName) ?? "";

            if (value.IndexOf(matchText.ToString()) != -1)
            {
                return true;
            } 
            else
            {
                return false;
            }
        }
        #endregion

        #region 转换类型
        public static Object Timestamp2StrTime(Context ctx, ParamList paramList)
        {
            if (ctx.Args == null || paramList.ParamCount == 0)
            {
                logger.Error("参数错误");
                return string.Empty;
            }

            var colName = paramList.GetParamAsString(0) ?? "";
            var value = GetValue(ctx.Args, colName) ?? "";

            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }

            long timestamp;
            bool ok = long.TryParse(value, out timestamp);
            if(timestamp <= 0 )
            {
                return timestamp.ToString();
            }

            if (ok)
            {
                var timestampType = paramList.GetParamAsInt(1) ?? 0;
                if(timestampType > 0)
                {
                    timestamp /= 1000;
                }
                return Common.Common.GetDateTime(timestamp).ToString(@"yyyy-MM-dd HH:mm:ss");
            }
            else
            {
                return value;
            }
        }
        #endregion
    }
}
