﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySqlDataView.Logic {
    public  class Config {

        public string Title { get; set; }
        public List<Product> Products {
            get;set;
        }
    }
}
