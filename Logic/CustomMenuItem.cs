﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MySqlDataView.Logic
{
    public class CustomMenuItem
    {
        public string Title { get; set; }
        
        public string SQL { get; set; }
    }
}