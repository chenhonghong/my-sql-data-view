﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MySqlDataView {
    /// <summary>
    /// UIHtmlView.xaml 的交互逻辑
    /// </summary>
    public partial class UIHtmlView : Window {
        public UIHtmlView() {
            InitializeComponent();
        }

        public UIHtmlView( string htmlText ) {
            InitializeComponent();

            // var lines = htmlText.Split(new char[] { '\n' }).Select(item => "<p>" + item + "</p>").ToArray() ;
            // var htmlContent = string.Join("", lines);

            htmlText = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"></head><body style='font-family:微软雅黑;margin:0;padding:0;overflow:hidden;'>" +
                          "<textarea style='width:100%;height:100%;font-family: \"Microsoft YaHei\", sans-serif;'>" + htmlText + "</textarea>" +
                       "</body></html>";
            WebView.NavigateToString( htmlText );
        }
    }
}
