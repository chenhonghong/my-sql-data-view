﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace MySqlDataView.Common
{
    public class Consts
    {
        public static SolidColorBrush DEFAULT_TEXT_COLOR = Brushes.DarkBlue;
        
        public const string ITEM = "Item";
        public const string CUSTOM_MENU = "CustomMenu";
        public const string CUSTOM_MENU_ITEM = "CustomMenuItem";
        
        public const string ATTR_CUSTOM_MENU_TITLE = "Title";
        public const string ATTR_CUSTOM_MENU_SQL = "SQL";
    }
}
