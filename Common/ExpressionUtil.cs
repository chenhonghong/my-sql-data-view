﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace MySqlDataView.Common {
    class ExpressionUtil {
        private static Regex expRegex = new Regex( @"^\$\{(.+)\}$" );
        private static Regex expContentRegex = new Regex( @"^(.+?):(.+)$" );


        public static string parseExp( string expString ) {
            var match = expRegex.Match( expString );
            if( !match.Success ) {
                return expString;
            }
            var expContent = match.Groups[ 1 ].ToString();
            var contentMatch = expContentRegex.Match( expContent );
            if( !contentMatch.Success ) {
                return expString;
            }
            var left = contentMatch.Groups[ 1 ].ToString().ToLower();
            var right = contentMatch.Groups[ 2 ].ToString();
            if( left != "date" ) {
                return expString;
            }
            var value = Convert.ToInt32( right );
            var now = DateTime.Now.AddDays( value );
            return now.ToShortDateString();
        }
    }
}
